FROM registry.gitlab.com/kimvanwyk/python3-poetry:latest

COPY ./home_hawks_key_point_roster/home_hawks_key_point_roster_site/ /app/
COPY run.sh /app

VOLUME /storage

ENV STORAGE_DIR=/storage

ENTRYPOINT ["bash", "run.sh"]
