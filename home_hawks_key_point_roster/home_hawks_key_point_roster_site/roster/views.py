import arrow
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import render

from collections import defaultdict
import copy

from .models import KeyPoint
from .models import Shift


def index(request):
    now = arrow.now()
    if now.hour % 2:
        now = now.shift(hours=-1)
    now = now.replace(minute=0, second=0, microsecond=0)
    shifts = []
    start = copy.copy(now)
    final = now.shift(hours=72)
    while start < final:
        end = start.shift(hours=2)
        keypoint_frequency = defaultdict(int)
        for shift in Shift.objects.filter(start=start.naive):
            keypoint_frequency[shift.keypoint] += 1
        keypoints = [
            k
            for k in KeyPoint.objects.order_by("name")
            if keypoint_frequency.get(k, 0) < k.shift_size
        ]
        shifts.append(
            (
                f"{start:DDMMYYTHHmm}",
                f"{start:ddd DD/MM/YY HH:mm}-{end:HH:mm}",
                keypoints,
            )
        )
        start = copy.copy(end)
    return render(request, "roster/shift_signup.html", {"shifts": shifts})


def shifts_submitted(request):
    shifts = [
        (arrow.get(shift[3:], "DDMMYYTHHmm"), KeyPoint.objects.get(pk=kp_id))
        for (shift, kp_id) in request.POST.items()
        if "kp_" in shift and kp_id != "-1"
    ]
    for (start, keypoint) in shifts:
        shift = Shift(
            name=request.POST["name"],
            phone=request.POST["phone"],
            keypoint=keypoint,
            start=start.naive,
            end=start.shift(hours=2).naive,
        )
        shift.save()
    if shifts:
        msg = f"Thank you for committing to {len(shifts)} shift{'s' if len(shifts) > 1 else ''} to help ensure key points in the North Durban area are monitored.<p/>Please go to the key point you have signed up for at your selected time."
    else:
        msg = "No shifts were selected."
    return HttpResponse(
        f"{msg}<p><a href='/'>Please click here if you would like to sign up for more shifts.</a>"
    )


@login_required
def roster(request):
    keypoints = [k.name for k in KeyPoint.objects.order_by("name")]

    now = arrow.now()
    if now.hour % 2:
        now = now.shift(hours=-1)
    now = now.replace(minute=0, second=0, microsecond=0)
    start = copy.copy(now)
    final = now.shift(hours=72)
    shifts = {}
    while start < final:
        end = start.shift(hours=2)
        shifts[f"{start:ddd DD/MM/YY HH:mm}-{end:HH:mm}"] = {k: [] for k in keypoints}
        start = copy.copy(end)
    for shift in Shift.objects.all().order_by("start"):
        time = (
            f"{arrow.get(shift.start):ddd DD/MM/YY HH:mm}-{arrow.get(shift.end):HH:mm}"
        )
        if time in shifts:
            shifts[time][shift.keypoint.name].append(f"{shift.name} ({shift.phone})")
    return render(
        request, "roster/roster.html", {"shifts": shifts, "keypoints": keypoints}
    )
