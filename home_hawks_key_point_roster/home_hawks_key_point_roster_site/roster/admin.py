from django.contrib import admin

from .models import KeyPoint
from .models import Shift

admin.site.register(KeyPoint)
admin.site.register(Shift)
