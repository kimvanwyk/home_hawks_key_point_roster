from django.urls import path

from . import views

urlpatterns = [
    path("", views.index, name="index"),
    path("signup", views.shifts_submitted, name="shifts_submitted"),
    path("roster", views.roster, name="roster"),
]
