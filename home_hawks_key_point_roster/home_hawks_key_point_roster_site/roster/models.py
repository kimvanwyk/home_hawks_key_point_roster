from django.db import models


class KeyPoint(models.Model):
    name = models.CharField(max_length=200)
    shift_size = models.IntegerField(default=2)

    def __str__(self):
        return self.name


class Shift(models.Model):
    name = models.CharField(max_length=200)
    phone = models.CharField(max_length=20)
    keypoint = models.ForeignKey(KeyPoint, on_delete=models.CASCADE)
    start = models.DateTimeField("start time")
    end = models.DateTimeField("end time")

    def __str__(self):
        return f"{self.keypoint} - {self.start:%d/%m/%y %H:%M}-{self.end:%H:%M} - {self.name}"
